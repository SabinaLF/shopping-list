import React from "react";
import "./style/index.scss";
import { useAppContext } from "./context/app-context";
import Body from "./components/Body";
import Header from "./components/Header";

const App = () => {
  const [{ isError }] = useAppContext();

  if (isError) {
    return <div className="error">Something went wrong ...</div>;
  }
  return (
    <>
      <Header />
      <Body />
    </>
  );
};

export default App;
