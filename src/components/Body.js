import React from 'react';
import { useAppContext } from '../context/app-context';
import { useFetchData } from '../utilities/helpers';

import Cards from './Cards';
import Categories from './Categories';
import Copyright from './Copyright';
import Modal from '../utilities/Modal';


const Body = () => {
  const [{ products, query, isModalOpen }] = useAppContext();
  const list = products.map(c => {
    return (
        <div key={c.id}className='list-item'>
            <div className='list-item-image'><img className='list-image' src={c.image} alt={c.name} /></div>
            <div className='list-item-details'>
              <p className='list-name'>{c.name}</p>
              {c.description && <p className='list-description'>{c.description}</p>}
              <p>£{c.price}</p>
            </div>
        </div >)
  })
  useFetchData(query);

  return (
    <div className="Body">
      <Categories />
      <Modal open={isModalOpen} children={list} />
      <Cards />
      <Copyright />
    </div>
  )
}
export default Body;