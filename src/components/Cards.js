import React from 'react';
import { useAppContext } from '../context/app-context';
import { useCardsFetchData } from '../utilities/helpers';

const Cards = () => {
    const [{ cardsProducts, cardsQuery }] = useAppContext();

    useCardsFetchData(cardsQuery);
    const card = cardsProducts.map(c => {
        return (
            <div key={c.id} className="card">
                <div className="bg-clip">
                    <div className="card-bg">
                        <p className="card-price">£{c.price}</p>
                        <div className="card-image"><img src={c.image} alt={c.name} /></div>
                    </div>
                </div>
                <div className="card-no-bg">
                    <p className="card-name">{c.name}</p>
                    {c.description!==undefined && <p className="card-description">{c.description.join('. ')}</p>}
                </div>
            </div >)
    })

    return (
        <div className="Cards">
            <div className="title">{cardsQuery}</div>
            <div className="cards-list">{card}</div>
        </div>
    )
}
export default Cards;