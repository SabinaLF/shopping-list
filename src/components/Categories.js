import React, { useState, useEffect } from 'react';
import uuid from 'uuid/v4';
import Category from './Category';

const Categories = () => {
    const [superDepartment] = useState(["fresh", "dried", "liquid", "frozen"]);
    const [productsCategories, setProductsCategories] = useState([])

    useEffect(() => {
        setProductsCategories(superDepartment.map(s => <Category key={uuid()} productCategory={s} />));
    }, [superDepartment])

    return (
        <div className="Categories">
            <div className="title">Products Collection</div>
            <div className="list">{productsCategories}</div>
        </div>
    )
}
export default Categories;