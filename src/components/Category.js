import React from 'react';
import { useAppContext } from '../context/app-context';

const Category = ({ productCategory }) => {
    const [, dispatch] = useAppContext()

    return (
        <div id={productCategory} className="Category" onClick={() => {
            dispatch({ type: 'SET_QUERY', query: productCategory })
            dispatch({ type: 'SET_CARDS_QUERY', cardsQuery: productCategory })
        }}>
            <div className="category-bg">
                <img className="category-img" src={`../img/${productCategory}.png`} alt={productCategory} />
            </div>
            <div className="category-no-bg">
                <p className="category-name">{productCategory}</p>
            </div>
        </div>
    )
}
export default Category;