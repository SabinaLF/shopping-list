import React from 'react';

const Copyright = () => {
  return (
    <div className="Copyright">
         <i className="material-icons copyright-icon">copyright</i> 
         <p className="copyright-text">2020 Sabina Lo Forte - All rights reserved.</p>
    </div>
  )
}
export default Copyright;