import React from "react";
import { useAppContext } from "../context/app-context";
import logo from "../img/logo.png";
import search from "../img/search.png";

import Input from "./Input";

const Header = () => {
  const [{ query, isLogin }, dispatch] = useAppContext();

  const handleOnClick = () => {
    dispatch({ type: "SET_CARDS_QUERY", cardsQuery: query });
    dispatch({ type: "SET_MODAL_OPEN", isModalOpen: false });
  };
  const handleLogin = () => {
    dispatch({ type: "SET_IS_LOGIN", isLogin: true });
  };

  return (
    <div className="Header">
      <div className="Input">
        <Input id="id_input" />
        <img
          id="id_search"
          src={search}
          className="search-icon"
          onClick={handleOnClick}
          alt="Search"
        />
      </div>

      <div className="header-title">
        <h1>Tesco shopping list</h1>
        <img className="header-logo" src={logo} alt="Logo" />
      </div>

      <div className="header-button">
        {!isLogin && (
          <div className="login-button" onClick={handleLogin}>
            Login
          </div>
        )}
        {isLogin && (
          <div>
            <div>£ 25.00</div>
            <i className="material-icons">shopping_cart</i>
          </div>
        )}
      </div>
    </div>
  );
};

export default Header;
