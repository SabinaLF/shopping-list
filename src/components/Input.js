import React from "react";
import { useAppContext } from "../context/app-context";

const Input = () => {
  const [, dispatch] = useAppContext();

  const handleQueryChange = (e) => {
    if (e.target.value !== "") {
      dispatch({ type: "SET_QUERY", [e.target.name]: e.target.value });
      dispatch({ type: "SET_MODAL_OPEN", isModalOpen: true });
    }
    if (e.target.value === "") {
      dispatch({ type: "SET_QUERY", [e.target.name]: e.target.value });
      dispatch({ type: "SET_MODAL_OPEN", isModalOpen: false });
    }
  };

  return (
    <input
      type="text"
      id="id_input"
      className="search-input"
      name="query"
      onChange={handleQueryChange}
      autoComplete="off"
    />
  );
};
export default Input;
