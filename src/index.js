import React from "react";
import ReactDOM from "react-dom";
import AppContextProvider from "./context/app-context";
import App from "./App";
import * as serviceWorker from "./serviceWorker";
import { initialState, reducer } from "./reducers";

const Root = () => {
  return (
    <AppContextProvider state={initialState} reducer={reducer}>
      <App />
    </AppContextProvider>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));
serviceWorker.unregister();
