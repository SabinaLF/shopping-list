// prettier-ignore
export default function AppReducer(state, action) {
    switch (action.type) {
      case 'SET_CARDS_PRODUCTS': return {...state, cardsProducts: action.cardsProducts};
      case 'SET_CARDS_QUERY': return {...state, cardsQuery: action.cardsQuery};
      case 'SET_IS_ERROR': return {...state, isError: action.isError};
      case 'SET_IS_LOADING': return {...state, isLoading: action.isLoading};
      case 'SET_IS_LOGIN': return {...state, isLogin: action.isLogin};
      case 'SET_MODAL_OPEN': return {...state, isModalOpen: action.isModalOpen};
      case 'SET_PRODUCTS': return {...state, products: action.products};
      case 'SET_QUERY': return {...state, query: action.query};
      default: return state;
    }
  }
