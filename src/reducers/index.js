import AppReducer from "./AppReducer";

export const initialState = {
  query: "fresh",
  cardsQuery: "fresh",
  isLoading: false,
  isError: false,
  isLogin: false,
  products: [],
  cardsProducts: [],
  isModalOpen: false,
};

export const reducer = (state, action) => AppReducer(state, action);
