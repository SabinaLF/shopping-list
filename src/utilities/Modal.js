import React from 'react';
import { useAppContext } from '../context/app-context';

const Modal = ({open, children}) => {
    const [, dispatch] = useAppContext();
    const style = () => (open ? {display: 'block'} : {display: 'none'});
    const handleIsOpen = () => {dispatch({ type: 'SET_MODAL_OPEN', isModalOpen: false });}
  return (
    <>
        <div className="wrapper" style={style()} open={open}>{children}</div>
        <div className="backdrop" style={style()} open={open} onClick={handleIsOpen}></div>
    </>
  )
}
export default Modal;