import { useEffect } from "react";
import axios from "axios";
import { useAppContext } from "../context/app-context";

export const useFetchData = (term) => {
  const [, dispatch] = useAppContext();
  useEffect(() => {
    if (term !== "") {
      const fetchData = async (query) => {
        dispatch({ type: "SET_IS_ERROR", isError: false });
        try {
          const TESCO_API_KEY = process.env.REACT_APP_TESCO_API_KEY;
          const response = await axios({
            method: "get",
            url: `https://dev.tescolabs.com/grocery/products/?query= + ${query} + &offset=0&limit=8&`,
            headers: { "Ocp-Apim-Subscription-Key": TESCO_API_KEY },
          });
          dispatch({
            type: "SET_PRODUCTS",
            products: response.data["uk"]["ghs"]["products"]["results"],
          });
        } catch (error) {
          dispatch({ type: "SET_IS_ERROR", isError: true });
        }
      };
      fetchData(term);
    }
  }, [term, dispatch]);
};

export const useCardsFetchData = (term) => {
  const [, dispatch] = useAppContext();
  useEffect(() => {
    if (term !== "") {
      const fetchData = async (query) => {
        dispatch({ type: "SET_IS_ERROR", isError: false });
        try {
          const TESCO_API_KEY = process.env.REACT_APP_TESCO_API_KEY;
          const response = await axios({
            method: "get",
            url: `https://dev.tescolabs.com/grocery/products/?query= + ${query} + &offset=0&limit=8&`,
            headers: { "Ocp-Apim-Subscription-Key": TESCO_API_KEY },
          });
          dispatch({
            type: "SET_CARDS_PRODUCTS",
            cardsProducts: response.data["uk"]["ghs"]["products"]["results"],
          });
        } catch (error) {
          dispatch({ type: "SET_IS_ERROR", isError: true });
        }
      };
      fetchData(term);
    }
  }, [term, dispatch]);
};
